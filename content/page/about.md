---
title: About
subtitle: The "Shared Notes" Project
comments: false
---

[gplv3]: https://www.gnu.org/licenses/gpl-3.0.html

All content is shared under [GPL v3][gplv3].

See the git history of this repository for older versions of articles. When an article is updated,
the publication date at the top will be updated. If you need a static version of an article, either
note the date you linked or fork this repo and link to your forked content.
