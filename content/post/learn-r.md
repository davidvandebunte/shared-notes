---
title: "Learn R"
date: 2021-07-24
tags: ['learn']
---

[lbe]: {{< relref "learn-by-example.md" >}}

Specializes [Learn by example][lbe].

## Cost

[itr]: https://cran.r-project.org/doc/manuals/r-release/R-intro.pdf
[rgf]: https://stackoverflow.com/a/7141669/622049
[cad]: https://stackoverflow.com/a/15059327/622049

Start with [An Introduction to R][itr]. The pdf version is more readable than the html version,
likely because the authors of R are from the academic world and are rendering html as an
afterthought (i.e. with LaTeX).

Functional programming resources:
- [Select a function from the apply family][rgf]
- [Call apply-like function on each row of a dataframe][cad]
