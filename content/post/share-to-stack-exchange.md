---
title: "Share to Stack Exchange"
date: 2020-12-07T11:19:16-06:00
tags: ['share']
---

[sn]: {{< relref "share-notes.md" >}}

Generalizes [Share Notes][sn].

## Cost

You'll need to reformulate your comments as a question. Is this so bad? You can fit almost any
content you want to share into the question format.
