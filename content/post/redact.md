---
title: "Redact"
date: 2021-07-10T13:39:47-05:00
tags: ['notes', 'share']
---

[sn]: {{< relref "share-notes.md" >}}

Part of [Share Notes][sn].

# Personal data

[pd]: https://en.wikipedia.org/wiki/Personal_data
[dra]: https://en.wikipedia.org/wiki/Data_re-identification

Protect [Personal data][pd], to reduce the effectiveness of the [Data re-identification][dra]
algorithms targeting you on the dark web. If they can identify you as an individual, they can start
to build up a network of information about you in order to hack one of your accounts.

Your username is one example of PII. If someone has your username on one website and your password
on that website, they can use it to try to get into your account on other websites (if your username
is always the same). PII is any information that can identify, contact, or locate a single person.
Would you share tax documents publicly?

Many websites still rely on a security model that depends on security questions. Someone can go onto
Facebook and find out the answers to these typical questions: In what city was your high school?
What was your childhood nickname?

# Jargon

[mtau]: https://en.wikipedia.org/wiki/Wikipedia:Make_technical_articles_understandable

You sometimes need to dumb down or generalize articles to a wider audience. That is, you need to
explain to grandmother when you would otherwise only have to have explained to:
1. Yourself
1. A coworker
1. An engineer from another company in your same field
1. A technical person
1. Your sister
1. Your dad

Keep technical levels high so you don't make your notes more verbose and therefore less useful to
you. See also [Wikipedia: Make technical articles understandable][mtau].

# Team data

[stow]: https://en.wikipedia.org/wiki/Security_through_obscurity#Obscurity_in_architecture_vs._technique
[oac]: https://openai.com/charter/

NIST recommends security through obscurity as one layer of a valid security tool; see [Security
through obscurity][stow]. Iran doesn’t know how to build a nuclear bomb because they lack technical
knowledge, among other reasons. The [OpenAI Charter][oac] includes a similar sentiment:
> We are committed to providing public goods that help society navigate the path to AGI. Today this
> includes publishing most of our AI research, but we expect that safety and security concerns will
> reduce our traditional publishing in the future, while increasing the importance of sharing
> safety, policy, and standards research.

Many companies have open source rules that don’t even allow you to use their hardware on open source
projects (unless it’s just a bug fix, for example). Most companies take security of their IP
seriously (you've been through several training sessions) and you should as well.

From a company’s perspective, ideas don’t have to be ground-breaking (or often even novel) to be
worth keeping private. The IP system (patents) was intended to protect against competition, while
still encouraging sharing.

# TODo

Search for "TODo" in your notes to confirm you aren't publishing incomplete work. That final o is
lowercase so this doesn't show up.
