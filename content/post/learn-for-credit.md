---
title: "Learn for credit"
date: 2021-07-24
tags: ['learn']
---

[learn]: {{< relref "learn.md" >}}
[lfd]: {{< relref "learn-from-data.md" >}}
[rl]: https://en.wikipedia.org/wiki/Reinforcement_learning

See [Reinforcement learning][rl]. Specializes [learn][learn] and arguably [Learn from data][lfd].
