---
title: "Share Comment"
date: 2021-07-24
tags: ['share']
---

[sn]: {{< relref "share-notes.md" >}}

Specializes [Share Notes][sn].

## Test

Comment on other articles, [StackExchange][se], or by writing an article that is essentially a large
comment on another article.

[se]: https://stackexchange.com/

## Value

You'll likely get a response from the original author.

## Cost

Comments can usually be deleted or directed to a smaller audience (even one person).

If you see something, say something.
