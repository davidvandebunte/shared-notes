---
title: "Learn by example"
date: 2021-07-11T14:07:05-05:00
tags: ['learn']
---

[lfc]: {{< relref "learn-for-credit.md" >}}
[sl]: https://en.wikipedia.org/wiki/Supervised_learning

See [Supervised learning][sl]. Arguably a special case of [Learn for credit][lfc], where the
"credit" is your score on a prediction-only test (getting examples right).
