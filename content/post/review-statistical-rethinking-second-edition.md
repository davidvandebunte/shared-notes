---
title: "Statistical Rethinking"
date: 2021-11-08
subtitle: "2nd Edition"
tags: []
---

[rh]: https://davidvandebunte.gitlab.io/-/rethinking/-/jobs/1755960065/artifacts/html/work/review.html
[ph6]: https://davidvandebunte.gitlab.io/-/rethinking/-/jobs/1755960065/artifacts/html/work/practice-6-to-11.html
[ph12]: https://davidvandebunte.gitlab.io/-/rethinking/-/jobs/1755960066/artifacts/html/work/practice-12.html
[ph13]: https://davidvandebunte.gitlab.io/-/rethinking/-/jobs/1755960067/artifacts/html/work/practice-13.html
[ph13t]: https://davidvandebunte.gitlab.io/-/rethinking/-/jobs/1755960068/artifacts/html/work/practice-13-trolley.html
[drh]: https://github.com/davidvandebunte/rethinking
[pdrl]: https://gitlab.com/davidvandebunte/rethinking/-/pipelines

For a detailed review of the book see [review.html][rh], which may also be useful as an (incomplete)
errata.

For answers to practice questions, see:
- [practice6-to-11.html][ph6]
- [practice-12.html][ph12]
- [practice-13.html][ph13]
- [practice-13-trolley.html][ph13t]

The links above are generated from the code in [davidvandebunte/rethinking: GitHub][drh], using
continuous integration (CI) running in [GitLab][pdrl]. If you're familiar with docker you may find
the docker image useful for an R shell and a Jupyter environment.
