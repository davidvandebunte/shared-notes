---
title: "Share to Wikimedia"
date: 2020-12-07T11:19:06-06:00
tags: ['share']
---

[sn]: {{< relref "share-notes.md" >}}

Generalizes [Share Notes][sn].

# Value

In order of value.

## Fast Feedback

Let's say you post crappy content to Wikipedia. Expect to get feedback from the maintainers of the
page; at worst you need to revert what you added and you learned something (even how to edit on
Wikipedia).

## Language

The longer you spend in your own notes, the more tied you'll be to your own wording rather than
publicly available shared notes (like Wikipedia). Are you learning new words in your own notes? Are
you inventing words there is already another word for out there?

When you read Wikipedia, the theory others have developed often don't fit into your personal
theories. Should you develop personal theories in personal notes when you could be reading and
adding to theories others have developed? Don't give up when you see a theory you don't understand
in "their" shared notes.

# Cost

In order of cost.

## Speed

It's slower to type on Wikipedia (in a browser) than to type in plain text.

## Form

Wikipedia is fundamentally oriented around nouns rather than verbs. If you prefer functional
programming rather than an everything-is-a-noun language like Java, it's easier to think in verbs.
In fact, a verb-oriented site (like this one) provides a good complement to that approach because
you can assume links to verbs are internal to the site, and links to nouns are to Wikipedia.
