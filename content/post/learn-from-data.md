---
title: "Learn from data"
date: 2021-07-24
tags: ['learn']
---

[learn]: {{< relref "learn.md" >}}
[ml]: https://en.wikipedia.org/wiki/Machine_learning
[lfc]: {{< relref "learn-for-credit.md" >}}
[lbe]: {{< relref "learn-by-example.md" >}}

Specializes [Learn][learn]. The phrase "learn from data" (or "learn from") appears all over [Machine
learning][ml].

Generalizes:
- [Learn for credit][lfc] (arguably equivalent)
- [Learn by example][lbe]
