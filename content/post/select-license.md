---
title: "Select License"
date: 2020-12-02T12:46:04-06:00
tags: ['code', 'notes']
---

[sn]: {{< relref "share-notes.md" >}}

Part of [Share Notes][sn].

Selecting a license may be a trivial activity when it comes to notes. If you want to use any GPL
content (e.g. a picture of an assembly line in an article on performance optimization) then you also
have to release anything you have as GPL. Otherwise, you're going to be spending a lot of time
thinking about how to avoid GPL content. You can always relicense content later, removing the bits
that are GPL.

Why share everything GPL? If you can teach others something, you're making the world a better place,
and it may come back to you someday. Are you optimizing for global good? Or optimizing for the
chance you can live much longer because someone else can use your work to extend your lifetime
(perhaps in healthcare)? If you have enough money, help that person by giving them knowledge.
